from distutils.core import setup
setup(name='perso',
      version='0.1',
      py_modules=[
          'chemutilities',
          'pmfutilities',
          'climaticdate',
          'deltaTool'
          ],
      )
