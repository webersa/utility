import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import pandas as pd
import seaborn as sns
import sqlite3

def get_sourceColor(source=None, SOURCES_like=False):
    if SOURCES_like:
        color ={
                "Road traffic": "#595959",
                "Traffic": "#595959",
                "Traffic_ind": "#595959",
                "Primary traffic": "#595959",
                "Traffic_exhaust": "#595959",
                "Traffic_dir": "#444444",
                "Traffic_non-exhaust": "#444444",
                "Oil/Vehicular": "#595959",
                "Biomass_burning": "#538134",
                "Biomass_burning1": "#538134",
                "Biomass_burning2": "#538134",
                "Sulfate_rich": "#ff0000",
                "Nitrate_rich": "#0000cc",
                "Secondary inorganics": "#0000cc",
                "Secondary_biogenic": "#8c564b",
                "Biogenic SOA": "#8c564b",
                "Anthropogenic SOA": "#8c564b",
                "Marine/HFO": "#8c564b",
                "Aged seasalt/HFO": "#8c564b",
                "Marine_biogenic": "#fc564b",
                "HFO": "#70564b",
                "HFO (stainless)": "#70564b",
                "Marine": "#ffbf00",
                "Marin": "#ffbf00",
                "Salt": "#9cc3e6",
                "Aged_salt": "#6f2f9f",
                "Fungal spores": "#00af4f",
                "Primary_biogenic": "#00af4f",
                "Biogenique": "#00af4f",
                "Biogenic": "#00af4f",
                "Dust": "#ff6500",
                "Crustal_dust": "#ff6500",
                "Industrial": "#ff65ff",
                "Indus/veh": "#ff65ff",
                "Arcellor": "#ff65ff",
                "Siderurgie": "#ff65ff",
                "Plant debris": "#2aff80",
                "Plant_debris": "#2aff80",
                "Débris végétaux": "#2aff80",
                "Choride": "#80e5ff",
                "PM other": "#cccccc",
                "Traffic/dust (Mix)": "#333333",
                "SOA/sulfate (Mix)": "#6c362b",
                "nan": "#ffffff"
                }
    else:
        color = {
                "Traffic": "#000000",
                "Road traffic": "#000000",
                "Primary traffic": "#000000",
                "Traffic_ind": "#000000",
                "Traffic_exhaust": "#000000",
                "Traffic_dir": "#444444",
                "Traffic_non-exhaust": "#444444",
                "Oil/Vehicular": "#000000",
                "Road traffic/oil combustion": "#000000",
                "Biomass_burning": "#92d050",
                "Biomass burning": "#92d050",
                "Biomass_burning1": "#92d050",
                "Biomass_burning2": "#92d050",
                "Sulfate_rich": "#ff2a2a",
                "Sulfate rich": "#ff2a2a",
                "Nitrate_rich": "#217ecb", # "#ff7f2a",
                "Nitrate rich": "#217ecb", # "#ff7f2a",
                "Secondary inorganics": "#0000cc",
                "Secondary_biogenic": "#ff7f2a", # 8c564b",
                "Secondary biogenic": "#ff7f2a", # 8c564b",
                "Biogenic SOA": "#8c564b",
                "Anthropogenic SOA": "#8c564b",
                "Marine/HFO": "#a37f15", #8c564b",
                "Aged seasalt/HFO": "#8c564b",
                "Marine_biogenic": "#fc564b",
                "HFO": "#70564b",
                "HFO (stainless)": "#70564b",
                "Marine": "#33b0f6",
                "Marin": "#33b0f6",
                "Salt": "#00b0f0",
                "Seasalt": "#00b0f0",
                "Sea/road salt": "#00b0f0",
                "Fresh seasalt": "#00b0f0",
                "Aged_salt": "#97bdff", #00b0f0",
                "Aged seasalt": "#97bdff", #00b0f0",
                "Fungal spores": "#ffc000",
                "Primary_biogenic": "#ffc000",
                "Primary biogenic": "#ffc000",
                "Biogenique": "#ffc000",
                "Biogenic": "#ffc000",
                "Dust": "#dac6a2",
                "Mineral dust": "#dac6a2",
                "Crustal_dust": "#dac6a2",
                "Industrial": "#7030a0",
                "Industries": "#7030a0",
                "Indus/veh": "#5c304b",
                "Industry/traffic": "#5c304b", #7030a0",
                "Arcellor": "#7030a0",
                "Siderurgie": "#7030a0",
                "Plant debris": "#2aff80",
                "Plant_debris": "#2aff80",
                "Débris végétaux": "#2aff80",
                "Choride": "#80e5ff",
                "PM other": "#cccccc",
                "Traffic/dust (Mix)": "#333333",
                "SOA/sulfate (Mix)": "#6c362b",
                "Sulfate rich/HFO": "#8c56b4",
                "nan": "#ffffff"
                }
    color = pd.DataFrame(index=["color"], data=color)
    if source:
        if source not in color.keys():
            print("WARNING: no {} found in colors".format(source))
            return "#666666"
        return color.loc["color", source]
    else:
        return color

def get_sourcesCategories(profiles):
    """Get the sources category according to the sources name.

    Ex. Aged sea salt → Aged_sea_salt

    :profiles: list
    :returns: list

    """
    possible_sources = {
        "Vehicular": "Traffic",
        "VEH": "Traffic",
        "VEH ind": "Traffic_ind",
        "Traffic_exhaust": "Traffic_exhaust",
        "Traffic_non-exhaust": "Traffic_non-exhaust",
        "VEH dir": "Traffic_dir",
        "Oil/Vehicular": "Traffic",
        "Road traffic/oil combustion": "Traffic",
        "Road traffic": "Road traffic",
        "Road trafic": "Road traffic",
        "Road traffic/dust": "Traffic/dust (Mix)",
        "Bio. burning": "Biomass_burning",
        "Bio burning": "Biomass_burning",
        "Comb fossile/biomasse": "Biomass_burning",
        "BB": "Biomass_burning",
        "Biomass Burning": "Biomass_burning",
        "Biomass burning": "Biomass_burning",
        "BB1": "Biomass_burning1",
        "BB2": "Biomass_burning2",
        "Sulfate-rich": "Sulfate_rich",
        "Nitrate-rich": "Nitrate_rich",
        "Sulfate rich": "Sulfate_rich",
        "Nitrate rich": "Nitrate_rich",
        "Secondaire": "Secondary_biogenic",
        "Secondary bio": "Secondary_biogenic",
        "Secondary biogenic": "Secondary_biogenic",
        "Secondary organic": "Secondary_biogenic",
        "Secondaire organique": "Secondary_biogenic",
        "Secondary biogenic/sulfate": "SOA/sulfate (Mix)",
        "Marine biogenic/HFO": "Marine/HFO",
        "Secondary biogenic/HFO": "Marine/HFO",
        "Marine bio/HFO": "Marine/HFO",
        "Marin bio/HFO": "Marine/HFO",
        "Sulfate rich/HFO": "Marine/HFO",
        "Marine secondary": "Secondary_biogenic",
        "Marin secondaire": "Secondary_biogenic",
        "HFO": "HFO",
        "HFO (stainless)": "HFO",
        "Marin": "Secondary_biogenic",
        "Sea/road salt": "Salt",
        "Road salt": "Salt",
        "Sea salt": "Salt",
        "Seasalt": "Salt",
        "Fresh seasalt": "Salt",
        "Sels de mer": "Salt",
        "Aged sea salt": "Aged_salt",
        "Aged seasalt": "Aged_salt",
        "Aged seasalt": "Aged_salt",
        "Primary bio": "Primary_biogenic",
        "Primary biogenic": "Primary_biogenic",
        "Biogénique primaire": "Primary_biogenic",
        "Biogenique": "Primary_biogenic",
        "Biogenic": "Primary_biogenic",
        "Mineral dust": "Dust",
        "Mineral dust ": "Dust",
        "Resuspended dust": "Dust",
        "Dust": "Dust",
        "Crustal dust": "Dust",
        "Dust (mineral)": "Dust",
        "Dust/biogénique marin": "Dust",
        "AOS/dust": "Dust",
        "Industrial": "Industrial",
        "Industry": "Industrial",
        "Industrie": "Industrial",
        "Industries": "Industrial",
        "Industry/vehicular": "Industry/traffic",
        "Industry/traffic": "Industry/traffic",
        "Industries/trafic": "Industry/traffic",
        "Fioul lourd": "HFO",
        "Arcellor": "Industrial",
        "Siderurgie": "Industrial",
        "Débris végétaux": "Plant_debris",
        "Chlorure": "Chloride",
        "PM other": "Other"
        }
    s = [possible_sources[k] for k in profiles]
    return s

def get_site_typology():
    import collections
    
    site_typologie = collections.OrderedDict()
    site_typologie["Urban"] = ["Talence", "Lyon", "Poitiers", "Nice", "MRS-5av",
                               "PdB", "Aix-en-provence", "Nogent", "Poitiers",
                               "Lens-2011-2012", "Lens-2013-2014", "Lens", "Rouen"]
    site_typologie["Valley"] = ["Chamonix", "Passy", "Marnaz", "GRE-cb", "VIF", "GRE-fr"]
    site_typologie["Traffic"] = ["Roubaix", "STG-cle"]
    site_typologie["Rural"] = ["Revin", "Peyrusse", "ANDRA-PM10", "ANDRA-PM2.5"]

    site_typologie_SOURCES = collections.OrderedDict()
    site_typologie_SOURCES["Urban"] = [
        "LEN", "LY", "MRS", "NGT", "NIC", "POI", "PdB", "PROV", "TAL", "ROU"
    ]
    site_typologie_SOURCES["Valley"] = ["CHAM", "GRE"]
    site_typologie_SOURCES["Traffic"] = ["RBX", "STRAS"]
    site_typologie_SOURCES["Rural"] = ["REV"]

    for typo in site_typologie.keys():
        site_typologie[typo] += site_typologie_SOURCES[typo]

    return site_typologie


def _format_ions(text):
    map_ions = {
        "Cl-": "Cl$^-$",
        "Na+": "Na$^+$",
        "K+": "K$^+$",
        "NO3-": "NO$_3^-$",
        "NH4+": "NH$_4^+$",
        "SO42-": "SO$_4^{2-}$",
        "Mg2+": "Mg$^{2+}$",
        "Ca2+": "Ca$^{2+}$",
        "nss-SO42-": "nss-SO$_4^{2-}$"
    }
    if text in map_ions.keys():
        return map_ions[text]
    else:
        return text

def format_ions(text):
    if isinstance(text, list):
        mapped = [_format_ions(x) for x in text]
    elif isinstance(text, str):
        mapped = _format_ions(x)
    else:
        raise KeyError("`text` must be a string or a list of string")
    return mapped

class plot():
    
    def mainCompentOfPM(station, dateStart, dateEnd):
        '''
        Plot a stacked bar plot of the different constitutant of the PM
        '''
        COLORS = {
            "OM": "#008000",
            "EC": "#000000",
            "Cl-": "#59B2B2",
            "NO3-": "#0000FF",
            "SO42-": "#FF0000",
            "NH4+": "#FF8000",
            "Ca2+": "#CED770",
            "Other ions": "#710077",
            "Metals": "#804000",
            "Anhydrous monosaccharides": "#004000",
            "Organic acids": "#CE9E8E",
            "Polyols": "#A0A015",
            "Oxalate": "#7D0000",
            "MSA": "#2D00BB",
            "Glucose": "#4B8A08",
            "Cellulose": "#0B3B0B",
            "HULIS": "#58ACFA"
        }
        TEXTCOLORS = {
            "OM": "#000000",
            "EC": "#FFFFFF",
            "Cl-": "#000000",
            "NO3-": "#FFFFFF",
            "SO42-": "#000000",
            "NH4+": "#000000",
            "Ca2+": "#000000",
            "Other ions": "#FFFFFF",
            "Metals": "#FFFFFF",
            "Anhydrous monosaccharides": "#FFFFFF",
            "Organic acids": "#000000",
            "Polyols": "#000000",
            "Oxalate": "#FFFFFF",
            "MSA": "#FFFFFF",
            "Glucose": "#FFFFFF",
            "Cellulose": "#FFFFFF",
            "HULIS": "#000000"
        }

        ORGANICS = ["HULIS", "Anhydrous monosaccharides", "Polyols", "Organic acids", "Oxalate",
                    "MSA", "Glucose", "Cellulose"]

        TO_GROUP = {
            "Metals": [
                "Al", "As", "Cd", "Cr", "Cu", "Fe", "Mn", "Mo", "Ni", "Pb", "Rb", "Sb",
                "Se", "Sn", "Ti", "V", "Zn"
            ],
            "Anhydrous monosaccharides":  ["Levoglucosan", "Mannosan", "Galactosan"],
            "Polyols":  ["Arabitol", "Sorbitol", "Mannitol"],
            "Organic acids": [
                "Maleic", "Succinic", "Citraconic", "Glutaric", "Oxoheptanedioic",
                "MethylSuccinic", "Adipic", "Methylglutaric", "3-MBTCA", "Phtalic",
                "Pinic", "Suberic", "Azelaic", "Sebacic"
            ],
            "Other ions": [
                "Na+", "K+", "Mg2+",
            ]
        }

        TO_MICROGRAMME = ["OM", "EC", "HULIS"]

        conn = sqlite3.connect("/home/webersa/Documents/BdD/BdD_PM/db.sqlite")
        df = pd.read_sql(
            "SELECT * FROM values_all WHERE station IN ('{}');".format(station),
            con=conn
        )

        df.date = pd.to_datetime(df.date)
        df.set_index("date", inplace=True, drop=True)

        df = df[(dateStart < df.index) & (df.index < dateEnd)]


        # Metals = [
        #     "Al", "As", "Ba", "Cd", "Co", "Cr", "Cs", "Cu", "Fe", "La", "Mn",
        #     "Mo", "Ni", "Pb", "Rb", "Sb", "Se", "Sn", "Sr", "Ti", "V", "Zn"
        # ]


        dff = pd.DataFrame()
        for k in TO_GROUP.keys():
            df[k] = df[TO_GROUP[k]].sum(axis=1, min_count=1)
        
        # Get only the columns we have
        dff = df.reindex(TO_GROUP.keys(), axis=1)
        dff["OM"] = df["OC"]*1.8 
        to_keep = ["EC", "NO3-", "NH4+", "Cl-", "SO42-", "Ca2+", "Oxalate", "MSA",
                   "Glucose", "Cellulose", "HULIS"]
        for k in to_keep:
            if k in df.columns:
                dff[k] = df[k]
        dff.apply(pd.to_numeric)
        
        # Convert ng to µg
        for i in TO_MICROGRAMME:
            dff[i] *= 1000
        
        # 2 dataframes: one for the 'main' components, one for the organics
        df_proportion_perday = pd.DataFrame()
        nonorganics = list(set(dff.columns)-set(ORGANICS))
        for c in nonorganics:
            df_proportion_perday[c] = dff[c]/dff[nonorganics].sum(axis=1)

        df_proportion_OM_perday = pd.DataFrame()
        for c in dff.columns:
            if c in ORGANICS:
                df_proportion_OM_perday[c] = dff[c]/dff["OM"]

        d = pd.DataFrame(index=list(df_proportion_OM_perday.columns) +
                         list(df_proportion_perday.columns))

        d["other"] = df_proportion_perday.median()
        d["organics"] = df_proportion_OM_perday.median()
        d.loc[ORGANICS, "other"] = pd.np.nan
        df_mg_per_gOM = df_proportion_OM_perday.median() * 1000

        # Plot part
        order1 = ["OM", "EC", "Cl-", "NO3-", "SO42-", "NH4+", "Ca2+", "Other ions",
                  "Metals"]
        order2 = ORGANICS.copy()

        d = d.reindex(order1+order2)
        d.dropna(axis=0, how="all", inplace=True)

        OMidentified = df_proportion_OM_perday.median().sum() * 100
        dnormalize = d/d.sum() * 100

        # d1 = d["other"].reindex(order1, axis=0)
        # d2 = d["organics"].reindex(order2, axis=0)
        # d1 = d1/d1.sum()
        # d2 = d2/d2.sum()


        f, ax = plt.subplots(figsize=(9.5,7.5))
        dnormalize.T.plot.bar(
            stacked=True,
            color=dnormalize.index.map(COLORS).dropna(),
            rot=0,
            ax=ax,
        )

        xpos = {"other": 0, "organics": 1}
        texts = {"other": [], "organics": []}
        for xvar in ["other", "organics"]:
            val = dnormalize[xvar].reset_index().melt(id_vars=["index"])
            cumsum = 0
            for i, v in zip(val["index"], val["value"]):
                if pd.np.isnan(v):
                    continue
                cumsum += v
                if xvar == "other":
                    annot = ax.annotate("{}".format(format_ions(i)), 
                                       (xpos[xvar]-0.28, (cumsum -v/2) ),
                                       ha="right",
                                       va="center"
                                      )
                else:
                    text = "{}\n({:.2f} mg.g$_{{OM}}^{{-1}}$)".format(i, df_mg_per_gOM.loc[i])
                    if len(text)<40:
                        text = text.replace("\n", " ")

                    annot = ax.annotate(text, 
                                        (xpos[xvar]+0.28, (cumsum -v/2) ),
                                        ha="left",
                                        va="center"
                                       )
                texts[xvar].append(annot)

                
                ax.annotate("{:.0f}%".format(v),
                            (xpos[xvar], (cumsum - v/2) ),
                            ha="center",
                            va="center",
                            color=TEXTCOLORS[i],
                            fontweight="bold"
                           )
        # texts = pd.Series(plt.gcf().get_children()[1].get_children())
        # idx = [type(t)==matplotlib.text.Annotation for t in texts]
        # texts = texts[idx].tolist()

        # adjust_text(texts["other"],
        #             arrowprops=dict(arrowstyle="->", color='r', lw=0.5),
        #             autoalign='', only_move={'points':'y', 'text':'y'}
        #
        #                                     )

        yOMidentified = OMidentified * dnormalize.loc["OM", "other"]/100
        ax.annotate("{:.0f}% identified".format(OMidentified),
                    (xpos["other"], yOMidentified/2),
                    ha="center",
                    va="center",
                    color="#FFFFFF",
                    fontweight="bold"
                   )
        ax.plot([0.25, 0.75], [yOMidentified, 100], "-k")
        ax.plot([-0.25, 0.25], [yOMidentified, yOMidentified], '-w')

        ax.set_title(station, fontsize=16)
        ax.set_xticklabels([])
        f.subplots_adjust(top=0.88,
                         bottom=0.11,
                         left=0.125,
                         right=0.85,
                         hspace=0.2,
                         wspace=0.2)
        ax.legend('', frameon=False)
        ax.yaxis.set_major_formatter(FuncFormatter('{0:.0f}%'.format))
        sns.despine()

