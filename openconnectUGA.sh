# In order to use the VPN of the UGA, we need a VPN client.  The one offered by
# default (CISCO), doesn't suit me for various reasons (propriatary, bug, install 
# things in /opt, "black box" ...).
# Here is a alternative, using openconnect, a free (as in beer and as in freedom)
# software release under the GPL v2.1.
# More info at http://www.infradead.org/openconnect/index.html.

# First, ensure you have openvpn and openconnect installed.
# On a debian based OS, you can install them with
#
#   $ apt-get install openvpn openconnect

# Here is your username (Agalan), you must change it to yours.
USER="webersa"
# default passerelle for the UGA
SERVER="vpn.grenet.fr"
# Interface name. Change it if you want to. No big deal.
TUN="tun1"
# shasum of the SSL certificate. In order to avoid typing "yes" every times.
SHASUM="dbb9635afac2918026d03c67fd5cb7b0531133d0d2c8fc7c9d92102e9a9735c9"

# Then, create a new tunnel interface (you will need to enter the root password)
sudo /usr/sbin/openvpn --mktun --dev $TUN
# and connect to it
sudo /usr/sbin/openconnect -u $USER $SERVER \
    --interface=$TUN \
    --servercert sha256:$SHASUM
# type ctrl+C to end the session

# ... then remove the interface
sudo /usr/sbin/openvpn --rmtun --dev $TUN
