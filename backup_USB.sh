#!/bin/bash

function getGitFolder(){
    find $1 -name .git | sed 's/\/.git//g' > "$2"
}

SRC="$HOME/"
DST="/media/webersa/PhD_Samuel/"
EXCLUDE="$HOME/bin/ExcludeRsync.txt"
EXCLUDEBASE="$HOME/bin/ExcludeRsyncBase.txt"
EXCLUDEGIT="$HOME/bin/ExcludeRsyncGitFolders.txt"


getGitFolder $SRC $EXCLUDEGIT

cat $EXCLUDEBASE > $EXCLUDE
cat $EXCLUDEGIT >> $EXCLUDE

DIR2SAVE=("Documents" "Images" "Vidéos" "bin" "Téléchargements")

# for dir in `ls "$SRC"`
for dir in "${DIR2SAVE[@]}"
do
    dir2save=$SRC$dir
    echo $dir2save
    rsync -a --progress --delete-after --log-file="rsync.log"\
        --exclude-from=$EXCLUDE\
        $dir2save $DST 
done
