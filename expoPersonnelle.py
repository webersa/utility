import pandas as pd
import sqlite3
import matplotlib.pyplot as plt
import seaborn as sns

plt.interactive(True)

def addweek(df):
    df["week"] = df.date.apply(lambda a: a.week)

conn = sqlite3.connect("/home/webersa/Documents/BdD/BdD_PM/db.sqlite")

perso = pd.read_sql(
    "SELECT * FROM values_all WHERE station in ('Cohorte_SEPAGES');",
    con=conn
)

outdoor = pd.read_sql(
    "SELECT * FROM values_all WHERE station in ('GRE-fr','GRE-cb', 'VIF');",
    con=conn
)

perso.date = pd.to_datetime(perso.date)
outdoor.date = pd.to_datetime(outdoor.date)
perso.set_index(["station", "date"], inplace=True, drop=False)
outdoor.set_index(["station", "date"], inplace=True, drop=False)

addweek(perso)
addweek(outdoor)


OPtype = "OP_DTT_m3"
years = [2016, 2017]# , 2018]

f, axes = plt.subplots(nrows=len(years), figsize=(14,8))
for i, year in enumerate(years):
    df = pd.DataFrame()
    persotmp = perso[perso.index.get_level_values("date").year == year]
    persotmp["OP_DTT_m3"] /= 1.29
    persotmp["OP_DTT_µg"] /= 1.29
    outdoortmp = outdoor[outdoor.index.get_level_values("date").year == year]
    persotmp["flag"] = "personnal"
    outdoortmp["flag"] = "outdoor"
    df = pd.concat([df,
                    persotmp[[OPtype, "week", "flag"]]
                   ]
                  )
    df = pd.concat([df,
                    outdoortmp[[OPtype, "week", "flag"]]
                   ]
                  )
    sns.boxplot(data=df.reset_index(), x="week", y=OPtype, hue="flag",
                ax=axes[i])
    axes[i].set_xticks(range(0,53))
    axes[i].set_xticklabels(range(1,54))
    axes[i].set_ylabel("{year}".format(year=year))
    axes[i].set_xlabel("Week number")
    axes[i].set_ylim([0,10.])

f.subplots_adjust(right=0.95, left=0.05)
f.suptitle(OPtype)
axes[0].legend(bbox_to_anchor=(1,1), loc="upper right", frameon=False)
axes[-1].legend('', frameon=False)
