import sys
import sqlite3
import pandas as pd


def getvaluesfromstation(station):
    BDDDIR = "/home/webersa/Documents/BdD/BdD_PM/db.sqlite"
    conn = sqlite3.connect(BDDDIR)

    df = pd.read_sql(
        "SELECT * FROM values_all WHERE station in ('{station}');".format(station=station),
        con=conn
    )

    ql = pd.read_sql(
        "SELECT * FROM QL WHERE station in ('{station}');".format(station=station),
        con=conn
    )
    conn.close()

    ql = ql.mean()
    for c in df.columns:
        if (df[c].dtype == float) and any(df[c]<0):
            df[c] = df[c].clip(ql.loc[c])

    return df


