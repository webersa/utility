import os
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

class plot():
    def plot_per_microgramm(df, profile, species):
        for p in profile:
            plt.figure(figsize=(12,4))
            ax = plt.gca()
            d = df.xs(p, level="profile")/(df.xs(p, level="profile").loc["PMrecons"].mean())
            d = d.reindex(species).unstack().reset_index()
            sns.boxplot(data=d.replace({0: pd.np.nan}), x="level_1", y=0, color="grey")
            ax.set_yscale('log')
            ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
            ax.set_ylim((10e-6,3))
            ax.set_ylabel("µg/µg")
            ax.set_xlabel("")
            ax.set_title(p)
            plt.subplots_adjust(left=0.1, right=0.9, bottom=0.3, top=0.9)
            if plot_save: plt.savefig(BDIR+p+"_profile_perµg.png")

    def plot_totalspeciesum(df, profile, species):
        sumsp = pd.DataFrame(columns=species, index=['sum'])
        for sp in species:
            sumsp[sp] = df.loc[(sp, slice(None)),:].mean(axis=1).sum()
        for p in profile:
            plt.figure(figsize=(12,4))
            ax = plt.gca()
            d = df.xs(p, level="profile").divide(sumsp.iloc[0], axis=0) * 100
            d = d.reindex(species).unstack().reset_index()
            sns.barplot(data=d, x="level_1", y=0, color="grey", ci="sd")
            ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
            ax.set_ylim((0,100))
            ax.set_ylabel("% of total specie sum")
            ax.set_xlabel("")
            ax.set_title(p)
            plt.subplots_adjust(left=0.1, right=0.9, bottom=0.3, top=0.9)
            if plot_save: plt.savefig(BDIR+p+"_profile.png")

    def plot_contrib(df, dfcontrib, profile, specie="PMrecons"):
        for p in profile:
            plt.figure(figsize=(12,4))
            ax = plt.gca()
            d = pd.DataFrame(columns = df.columns,
                             index=dfcontrib.index)
            for BS in df.columns:
                d[BS] = dfcontrib[p] * df.xs(p, level="profile").loc[specie][BS]
            mstd = d.std(axis=1)
            ma = d.mean(axis=1)
            plt.fill_between(ma.index, ma-mstd, ma+mstd, alpha=0.2)
            d.mean(axis=1).plot(marker="*")
            ax.set_ylabel("Contribution to {} ($µg.m^{{-3}}$)".format(specie))
            ax.set_xlabel("")
            ax.set_title(p)
            plt.subplots_adjust(left=0.1, right=0.9, bottom=0.2, top=0.9)
            if plot_save: plt.savefig(BDIR+p+"_contribution.png")


plt.interactive(True)

site = "SOURCES2"
BDIR = "/home/webersa/Documents/BdD/BdD_PMF/PdB/"

cdir = os.getcwd()
# os.chdir(BDIR)
plot_per_microgramm = True
plot_totalspeciesum = True
plot_contrib = True
plot_save = True



dfbase = pd.read_excel(BDIR+site+"_base.xlsx",
                       sheet_name=['Profiles'])["Profiles"]

profile = dfbase.iloc[1,:].dropna()
nprofile = len(profile)

dfcontrib = pd.read_excel(BDIR+site+"_Constrained.xlsx",
                          sheet_name=['Contributions'], 
                          parse_date=["date"])["Contributions"]

dfprofile_boot = pd.read_excel(BDIR+site+"_Gcon_profile_boot.xlsx",
                               sheet_name=['Profiles'],
                               header=nprofile+2+3)["Profiles"]
dfprofile_boot = dfprofile_boot.iloc[1:,13:]

species = dfbase.iloc[:,1].dropna().unique()
profile = dfbase.iloc[1,:].dropna()

dfcontrib.index = dfcontrib.iloc[:,0]
dfcontrib.drop('Factor Contributions (avg = 1) from Constrained Run (Convergent Run)',
               inplace=True,
              axis=1)

if "Factor Contributions (conc. units) from Constrained Run (Convergent Run)" in dfcontrib.index:
    dfcontrib = dfcontrib.loc[:"Factor Contributions (conc. units) from Constrained Run (Convergent Run)",:]
    dfcontrib = dfcontrib.iloc[:-2,:]
dfcontrib = dfcontrib.iloc[1:,:]
dfcontrib.columns = ["date"] + profile.tolist()
dfcontrib.replace({-999:pd.np.nan}, inplace=True)
dfcontrib.set_index("date", inplace=True)

def split_df_by_nan(df, species):    
    idxnan = df.iloc[:,1].isnull()
    n_factor = -1                    
    for i in idxnan:                 
        n_factor += 1                
        if i:                        
            break                    
    d = {}                           
    dftmp = df.dropna()              
    for i, sp in enumerate(species): 
        d[sp] = dftmp.iloc[n_factor*i:n_factor*(i+1),:]                   
        d[sp].index = profile
        d[sp].index.name = "profile"
        d[sp].columns = ["Boot{}".format(i) for i in range(len(d[sp].columns))]
    return d                         

df = split_df_by_nan(dfprofile_boot, species)

df = pd.concat(df)

if plot_per_microgramm:
    plot.plot_per_microgramm(df, profile, species)

if plot_totalspeciesum:
    plot.plot_totalspeciesum(df, profile, species)

if plot_contrib:
    plot.plot_contrib(df, dfcontrib, profile, specie="PMrecons")


os.chdir(cdir)
