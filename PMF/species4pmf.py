# -*- coding: utf-8 -*-
import pandas as pd
import sqlite3


DB = "/home/webersa/Documents/BdD/BdD_PM/aerosols.db"
SAVEFILE = "/home/webersa/bin/PMF/species4pmf.csv"

def get_species(PMFtype):
    if PMFtype == "SOURCES":
        carboneous = ["OC", "EC"]
        ions = ["Cl-", "NO3-", "SO42-", "Na+", "NH4+", "K+", "Mg2+", "Ca2+"]
        organics = [
            "MSA",
            "Arabitol", "Sorbitol", "Mannitol", "Levoglucosan", "Mannosan",
        ]

        metals = [
            "Al", "As", "Ba", "Cd", "Ce", "Co", "Cr", "Cs", "Cu", "Fe",
            "La", "Li", "Mn", "Mo", "Ni", "Pb", "Rb", "Sb",
            "Se", "Sn", "Sr", "Ti", "Tl", "V", "Zn", "Zr"
        ]

        keep_col = ["date", "PM10", "PMrecons"] + carboneous + ions + organics + metals

    elif PMFtype == "MOCOPO":
        carboneous = ["OC", "EC"]
        ions = ["Cl-", "NO3-", "SO42-", "Na+", "NH4+", "K+", "Mg2+", "Ca2+"]
        organics = [
            "Levoglucosan", "Mannosan",

            "Phe", "Ace", "For", "An", "Fla", "Pyr", "Tri", "Ret", "BaA", "Chr", "BeP", "BbF", "BkF",
            "BaP", "BghiP", "DBahA", "IP", "Cor"
        ]
        organics += ["C{}".format(i) for i in range(11,41)]
        organics += ["HP{c}".format(c=str(i)) for i in range(1,11)]

        metals = [
            "Cu", "Fe", "Sn", "Mn", "Sb", "Ba", "Ti", "Sr", "Zn", "Al", "Cr"
        ]

        keep_col = ["date", "PM10", "PM10 corrigées", "PM2.5 corrigées", "PMrecons"] + carboneous + ions + organics + metals

    elif PMFtype == "generic":
        carboneous = ["OC", "EC"]
        ions = ["Cl-", "NO3-", "SO42-", "Na+", "NH4+", "K+", "Mg2+", "Ca2+"]
        organics = [
            "MSA",
            "Arabitol", "Sorbitol", "Mannitol", "Levoglucosan", "Mannosan",
            "Polyols",
            "Galactosan", "Glucose", "Cellulose", "Oxalate", 
            
            "Maleic", "Succinic", "Citraconic", "Glutaric", "Oxoheptanedioic",
            "MethylSuccinic", "Adipic", "Methylglutaric", "3-MBTCA", "Phtalic",
            "Pinic", "Suberic", "Azelaic", "Sebacic",

            "PAH Sum", "Alkanes Sum", "MethPAH Sum", "PAHs Sum", "Hopane Sum",
            "Methoxyphenol Sum", "MNC Sum", "MC Sum",
            "BNT(2,1)", "BNT(1,2)", "BNT(2,3)", "DNT(2,1)", "BPT(2,1)",
            "Vanilline", "Acetovanillone", "Guaiacylacetone", "Coniferylaldehyde",
            "Vanillic", "Hamovanillic", "Syringol", "4-methylsyringol",
            "4-propenylsyringol", "Acetosyringone", "Syringyl acetone",
            "Sinapyl aldehyde", "Syringic acid", "Cholesterol",
            "Phe", "Ace", "For", "An", "Fla", "Pyr", "Tri", "Ret", "BaA", "Chr", "BeP", "BbF", "BkF",
            "BaP", "BghiP", "DBahA", "IP", "Cor"
        ]
        organics += ["C{}".format(i) for i in range(11,41)]
        organics += ["HP{c}".format(c=str(i)) for i in range(1,11)]

        metals = [
            "Al", "As", "Ba", "Ca", "Cd", "Ce", "Co", "Cr", "Cs", "Cu", "Fe", "K",
            "La", "Li", "Mg", "Mn", "Mo", "Na", "Ni", "Pb", "Pd", "Pt", "Rb", "Sb",
            "Sc", "Se", "Sn", "Sr", "Ti", "Tl", "V", "Zn", "Zr"
        ]

        keep_col = ["date", "PM10", "PMrecons"] + carboneous + ions + organics + metals

    return keep_col


def main():
    """Create a csv file with true/false for including species in PMF
    :returns: TODO

    """
    conn = sqlite3.connect(DB)
    df = pd.read_sql("SELECT * FROM values_all", con=conn)
    conn.close()

    d = pd.DataFrame(index=df.columns)
    
    PMFtypes = ["SOURCES", "MOCOPO", "generic"]
    functions = {

    }
    for PMFtype in PMFtypes: 
        species = get_species(PMFtype=PMFtype)
        d[PMFtype] = False
        d.loc[species, PMFtype] = True

    d.to_csv(SAVEFILE)


if __name__=="__main__":
    main()
