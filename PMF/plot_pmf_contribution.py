import sqlite3
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import seaborn as sns
import collections


plt.interactive(True)

def get_db(conn=None):
    if conn is None:
        conn = sqlite3.connect(
            "/home/samuel/Documents/IGE/BdD/bdd_aerosols/aerosols.db"
        )

    df_profiles = pd.read_sql(
        "SELECT * FROM PMF_profiles WHERE programme in ('SOURCES', 'SOURCES2')",
        con=conn
    )
    df_contributions = pd.read_sql(
        "SELECT * FROM PMF_contributions WHERE programme in ('SOURCES', 'SOURCES2')",
        con=conn,
        parse_dates=["date"]
    )

    df_profiles = df_profiles[df_profiles.station != "Pipiripi"]
    df_profiles = df_profiles.set_index(["station", "specie"])\
            .drop(["index", "programme"], axis=1)

    df_contributions = df_contributions[df_contributions.station != "Pipiripi"]
    df_contributions = df_contributions.set_index(["station", "date"])\
            .drop(["index", "programme"], axis=1)

    df_profiles.dropna(axis=1, how="all", inplace=True)
    df_contributions.dropna(axis=1, how="all", inplace=True)

    return (df_profiles, df_contributions)

def to_microgrampercubicmeter(df_profiles, df_contributions):
    pm10 = df_profiles.loc[(slice(None), "PM10"), :]\
            .reset_index()\
            .set_index("station")\
            .drop(["specie"], axis=1)

    contrib = pm10 * df_contributions
    return contrib

def to_relativecontribution(contrib):
    relative = contrib.div(contrib.sum(axis=1), axis="index")
    return relative

def add_season(df):
    """
    Add a season column to the DataFrame df.

    parameters
    ----------

    df: Pandas DataFrame.
        The DataFrame to work with.

    return
    ------

    dfnew: a new pandas DataFrame with a 'season' columns.

    """

    month_to_season = {1:'Winter', 2:'Winter', 3:'Spring', 4:'Spring', 5:'Spring', 6:'Summer',
                       7:'Summer', 8:'Summer', 9:'Fall', 10:'Fall', 11:'Fall',
                       12:'Winter'}

    dfnew = df.copy()

    # ensure we have date in index
    if isinstance(dfnew.index, pd.DatetimeIndex):
        dfnew["date"] = dfnew.index
    elif 'date' in dfnew.columns:
        dfnew["date"] = pd.to_datetime(dfnew["date"])
    else:
        print("No date given")
        return

    # add a new column with the number of the month (Jan=1, etc)
    dfnew["month"] = dfnew.date.apply(lambda x: x.month)
    # sort it. This is not mandatory.
    dfnew.sort_values(by="month", inplace=True)

    # add the season based on the month number
    dfnew["season"] = dfnew["month"].replace(month_to_season)

    # and return the new dataframe
    return dfnew
  
profiles, contributions = get_db()
contrib = to_microgrampercubicmeter(profiles, contributions)
relative = to_relativecontribution(contrib) * 100
relative = add_season(relative.reset_index())
typology = collections.OrderedDict({
    "urban": ["NGT", "POI", "LY", "NIC", "MRS", "ROU", "LEN", "PdB", "PROV",
              "TAL"],
    "urban+valley": ["GRE", "CHAM", "Marnaz", "Passy"],
    "traffic": ["STRAS", "RBX"],
    "rural": ["REV"]
})

nrural = len(typology["rural"])
nurban = len(typology["urban"])
nvalley = len(typology["urban+valley"])
ntraffic = len(typology["traffic"])

order = typology["rural"] + typology["urban"] + typology["urban+valley"] + typology["traffic"]

f = plt.figure(figsize=(14,5))
sns.barplot(
    data=relative,
    hue="season",
    x="station",
    y="Biomass burning",
    ci=None,
    order=order,
    # palette=[sns.saturate(x) for x in ["#b2b2b2", "#7fcc7f", "#9fb6ff", "#ffd47f"]]
    palette=["#b2b2b2", "#7fcc7f", "#9fb6ff", "#ffd47f"]
)

ax = plt.gca()
xmin, xmax, ymin, ymax = ax.axis()
ax.set_ylim([0, ymax])
ymin = 0
ymax = 100
ax.axis([xmin, xmax, ymin, ymax])

alpha = 0.5

rect1 = Rectangle((xmin, 0), nrural, ymax, color="#dfffbf", alpha=alpha, zorder=-1)
rect2 = Rectangle((xmin+nrural, 0), nurban, ymax, color="#ffffbf", alpha=alpha, zorder=-1)
rect3 = Rectangle((xmin+nrural+nurban, 0), nvalley, ymax, color="#f2e1bf",
                  alpha=alpha, zorder=-1)
rect4 = Rectangle((xmin+nrural+nurban+nvalley, 0), ntraffic, ymax, color="#ffcfcf",
                  alpha=alpha, zorder=-1)

ax.add_patch(rect1)
ax.add_patch(rect2)
ax.add_patch(rect3)
ax.add_patch(rect4)

map_name = {
    "NGT": "Nogent",
    "REV": "Revin",
    "POI": "Poitiers",
    "ROU": "Rouen",
    "LY": "Lyon",
    "NIC": "Nice",
    "GRE": "Grenoble",
    "MRS": "Marseille",
    "LEN": "Lens",
    "PdB": "Port de bouc",
    "RBX": "Roubaix",
    "PROV": "Aix en provence",
    "CHAM": "Chamonix",
    "TAL": "Talence",
    "STRAS": "Strasbourg",
    "Marnaz": "Marnaz",
    "Passy": "Passy"
}
ax.set_xticklabels([map_name[x.get_text()] for x in ax.get_xticklabels()], rotation=90)
ax.set_xlabel("")
plt.tight_layout()
