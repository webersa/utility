#!/usr/bin/env python3
#-*- coding: utf-8 -*-
import sys
import argparse
import pandas as pd
import sqlite3

def get_value_from_db(conn, keep_col, site, table=None, programme="_%"):
    """
    Query the PM database
    """
    if (not table) or (table not in ['concentration', 'QL']):
        raise ValueError("'table' should be in ['concentration, 'QL']")
    if table == "concentration":
        table = 'values_all'
    elif table == "QL": 
        table = "QL"
        keep_col += ['sample ID']

    if not (type(programme) == type("string")) and (programme is not None):
        raise TypeError("'programme' shoud be a string")

    
    kwarg = {
        'sp': '", "'.join(keep_col),
        'what': table,
        'site': site,
        'programme': programme
    }
    if programme == "_%": 
        programme = None
    sqlquery = 'SELECT "{sp}" FROM {what} WHERE station IN ("{site}")'
    if programme is not None:
        sqlquery += ' AND programme LIKE "{programme}";'
    else:
        sqlquery += ';'

    df = pd.read_sql(
        sqlquery.format(
            sp=kwarg["sp"], what=kwarg["what"], site=kwarg["site"],
            programme=kwarg["programme"]
        ),
        con=conn
    )
    if table == "QL":
        df = df.loc[df["sample ID"].str.contains("QL"),:]
        df.drop("sample ID", axis=1, inplace=True)
    return df

def ql2value(df, qlval):
    for c in range(len(df.columns)):
        if c not in qlval.columns:
            raise KeyError("Columns {} not in the QL dataframe".format(c))
        if pd.notnull(qlval.iloc[0, c]):
            if pd.isnull(qlval.iloc[1, c]):
                ql = qlval.iloc[0, c]
            else:
                ql = (qlval.iloc[0, c] + qlval.iloc[1, c])/2
        elif pd.notnull(qlval.iloc[1, c]):
            ql = qlval.iloc[1, c]
        elif "<QL" in df.iloc[:,c]:
            raise ValueError("No QL for the specie {c}".format(c=df.columns[c]))
        else:
            continue
        df[df.columns[c]].replace({"<QL":ql/2, "<DL": ql/2}, inplace=True)

def compute_OCstar(df):
    # required = ['OC','MSA','Arabitol','Mannitol','Levoglucosan','Mannosan']
    # if not set(required).issubset(set(df.columns)):
    #     raise KeyError('The following species are required for OC*:{}'.format(required))
    OCstar = df['OC'].copy()
    equivC = {
        'Oxalate': 0.27,
        'Arabitol': 0.40,
        'Mannitol': 0.40,
        'Sorbitol': 0.40,
        'Levoglucosan': 0.44,
        'Mannosan': 0.44,
        'Galactosan': 0.44,
        'MSA': 0.12,
        'Glucose': 0.44,
        'Cellulose': 0.44,
        'Maleic': 0.41,
        'Succinic': 0.41,
        'Citraconic': 0.46,
        'Glutaric': 0.45,
        'Oxoheptanedioic': 0.48,
        'MethylSuccinic': 0.53,
        'Adipic': 0.49,
        'Methylglutaric': 0.49,
        '3-MBTCA': 0.47,
        'Phtalic': 0.58,
        'Pinic': 0.58,
        'Suberic': 0.55,
        'Azelaic': 0.57,
        'Sebacic': 0.59,
    }
    for sp in equivC.keys():
        if sp in df.columns:
            OCstar -= df[sp] * equivC[sp]

    return OCstar

def compute_WAX(df):
    wax=pd.DataFrame(index=df.index)
    for i in range(12,40):
        C = "C"+str(i)
        Cn = "C"+str(i-1)
        Cp = "C"+str(i+1)
        if C in df.columns:
            wax["WAX"+str(i)] = df[C] - 0.5*(df[Cn]-df[Cp]) 

    return wax[["WAX{}".format(i) for i in range(18,40)]].sum(axis=1)

def pretty_index(df):
    keep_col = get_all_species_pretty_index()
    keep_col.remove("date")
    df = df.reindex(keep_col, axis=1)
    df.dropna(how="all", axis=1, inplace=True)
    return df

def get_all_species_pretty_index():
    carboneous = ["OC", "OC*", "EC"]
    ions = ["Cl-", "NO3-", "SO42-", "Na+", "NH4+", "K+", "Mg2+", "Ca2+"]
    organics = [
        "MSA",
        "Arabitol", "Sorbitol", "Mannitol", "Levoglucosan", "Mannosan",
        "Polyols",
        "Galactosan", "Glucose", "Cellulose", "Oxalate", 
        
        "Maleic", "Succinic", "Citraconic", "Glutaric", "Oxoheptanedioic",
        "MethylSuccinic", "Adipic", "Methylglutaric", "3-MBTCA", "Phtalic",
        "Pinic", "Suberic", "Azelaic", "Sebacic",

        "PAH Sum", "Alkanes Sum", "MethPAH Sum", "PAHs Sum", "Hopane Sum",
        "Methoxyphenol Sum", "MNC Sum", "MC Sum",
        "BNT(2,1)", "BNT(1,2)", "BNT(2,3)", "DNT(2,1)", "BPT(2,1)",
        "Vanilline", "Acetovanillone", "Guaiacylacetone", "Coniferylaldehyde",
        "Vanillic", "Hamovanillic", "Syringol", "4-methylsyringol",
        "4-propenylsyringol", "Acetosyringone", "Syringyl acetone",
        "Sinapyl aldehyde", "Syringic acid", "Cholesterol",
        "Phe", "Ace", "For", "An", "Fla", "Pyr", "Tri", "Ret", "BaA", "Chr", "BeP", "BbF", "BkF",
        "BaP", "BghiP", "DBahA", "IP", "Cor"
    ]
    organics += ["C{}".format(i) for i in range(11,41)]
    organics += ["HP{c}".format(c=str(i)) for i in range(1,11)]
    organics += ["Alkanes WAX", "Alkanes noWAX"]

    metals = [
        "Al", "As", "Ba", "Ca", "Cd", "Ce", "Co", "Cr", "Cs", "Cu", "Fe", "K",
        "La", "Li", "Mg", "Mn", "Mo", "Na", "Ni", "Pb", "Pd", "Pt", "Rb", "Sb",
        "Sc", "Se", "Sn", "Sr", "Ti", "Tl", "V", "Zn", "Zr"
    ]

    keep_col = ["date", "PM10", "PM10 corrigées", "PM2.5 corrigées",  "PMrecons"] + carboneous + ions + organics + metals
    return keep_col

def get_all_species():
    carboneous = ["OC", "EC"]
    ions = ["Cl-", "NO3-", "SO42-", "Na+", "NH4+", "K+", "Mg2+", "Ca2+"]
    organics = [
        "MSA",
        "Arabitol", "Sorbitol", "Mannitol", "Levoglucosan", "Mannosan",
        # "Polyols",
        "Galactosan", "Glucose", "Cellulose", "Oxalate", 
        
        "Maleic", "Succinic", "Citraconic", "Glutaric", "Oxoheptanedioic",
        "MethylSuccinic", "Adipic", "Methylglutaric", "3-MBTCA", "Phtalic",
        "Pinic", "Suberic", "Azelaic", "Sebacic",

        # "PAH Sum", "Alkanes Sum", "MethPAH Sum", "PAHs Sum", "Hopane Sum",
        # "Methoxyphenol Sum", "MNC Sum", "MC Sum",
        "BNT(2,1)", "BNT(1,2)", "BNT(2,3)", "DNT(2,1)", "BPT(2,1)",
        "Vanilline", "Acetovanillone", "Guaiacylacetone", "Coniferylaldehyde",
        "Vanillic", "Hamovanillic", "Syringol", "4-methylsyringol",
        "4-propenylsyringol", "Acetosyringone", "Syringyl acetone",
        "Sinapyl aldehyde", "Syringic acid", "Cholesterol",
        "Phe", "Ace", "For", "An", "Fla", "Pyr", "Tri", "Ret", "BaA", "Chr", "BeP", "BbF", "BkF",
        "BaP", "BghiP", "DBahA", "IP", "Cor"
    ]
    organics += ["C{}".format(i) for i in range(11,41)]
    organics += ["HP{c}".format(c=str(i)) for i in range(1,11)]
    # organics += ["Alkanes WAX", "Alkanes noWAX"]

    metals = [
        "Al", "As", "Ba", "Ca", "Cd", "Ce", "Co", "Cr", "Cs", "Cu", "Fe", "K",
        "La", "Li", "Mg", "Mn", "Mo", "Na", "Ni", "Pb", "Pd", "Pt", "Rb", "Sb",
        "Sc", "Se", "Sn", "Sr", "Ti", "Tl", "V", "Zn", "Zr"
    ]

    keep_col = ["date", "PM10", "PM10 corrigées", "PM2.5 corrigées", "PMrecons"] + carboneous + ions + organics + metals
    return keep_col

def get_uncertainties():
    unc = {
        "PM10": {"type": "percent", "value": 10},
        "PM10 corrigées": {"type": "percent", "value": 10},
        "PM2.5 corrigées": {"type": "percent", "value": 10},
        "PMrecons": {"type": "percent", "value": 10},
        "PM2.5": {"type": "percent", "value": 10},
        "OC":   {"type": "percent", "value": 10},
        "OC*":  {"type": "percent", "value": 10},
        "EC":   {"type": "percent", "value": 10},
        # "MSA":  {"type": "gianini", "value": {"DL": float, "CV": 26.00, "a":0.05 }},
        "MSA":  {"type": "gianini", "value": {"DL": float, "CV": 10.00, "a":0.05 }},
        "Cl-":  {"type": "gianini", "value": {"DL": float, "CV": 6.00, "a":0.05 }},
        "NO3-": {"type": "gianini", "value": {"DL": float, "CV": 3.36, "a":0.05 }},
        "SO42-":{"type": "gianini", "value": {"DL": float, "CV": 11.1, "a":0.05 }},
        "Na+":  {"type": "gianini", "value": {"DL": float, "CV": 1.65, "a":0.05 }},
        "NH4+": {"type": "gianini", "value": {"DL": float, "CV": 15.66, "a":0.05 }},
        "K+":   {"type": "gianini", "value": {"DL": float, "CV": 7.45, "a":0.05 }},
        "Mg2+": {"type": "gianini", "value": {"DL": float , "CV": 10.96, "a":0.05 }},
        "Ca2+": {"type": "gianini", "value": {"DL": float , "CV": 24.69, "a":0.05 }},
        "Oxalate": {"type": "gianini", "value": {"DL": float , "CV": 26.00, "a":0.05 }},
        "Polyols": {"type": "gianini", "value": {"DL": float , "CV": 10, "a":0.10 }},
        "Levoglucosan": {"type": "gianini", "value": {"DL": float , "CV": 5.84, "a":0.10 }},
        "Mannosan": {"type": "gianini", "value": {"DL": float , "CV": 3.44, "a":0.10 }},
        "Galactosan": {"type": "gianini", "value": {"DL": float , "CV": 3.64, "a":0.10 }},
        "Arabitol": {"type": "gianini", "value": {"DL": float , "CV": 4.49, "a":0.10 }},
        "Sorbitol": {"type": "gianini", "value": {"DL": float , "CV": 3.21, "a":0.10 }},
        "Mannitol": {"type": "gianini", "value": {"DL": float , "CV": 3.98, "a":0.10 }},
        "Glucose": {"type": "gianini", "value": {"DL": float , "CV": 3.36, "a":0.10 }},

        "HP1": {"type": "percent", "value": 10},
        "HP2": {"type": "percent", "value": 10},
        "HP3": {"type": "percent", "value": 10},
        "HP4": {"type": "percent", "value": 10},
        "HP5": {"type": "percent", "value": 10},
        "HP6": {"type": "percent", "value": 10},
        "HP7": {"type": "percent", "value": 10},
        "HP8": {"type": "percent", "value": 10},
        "HP9": {"type": "percent", "value": 10},
        "HP10": {"type": "percent", "value": 10},
        "Pristane": {"type": "percent", "value": 10},
        "Phytane": {"type": "percent", "value": 10},
        
        "Cellulose": {"type": "gianini", "value": {"DL": float , "CV": 20.00, "a":0.05 }},
        "3-MBTCA": {"type": "gianini", "value": {"DL": float , "CV": 10.00, "a":0.05 }}, # CV arbitraire
        "Maleic": {"type": "gianini", "value": {"DL": float , "CV": 10.9, "a":0.05 }},
        "Succinic": {"type": "gianini", "value": {"DL": float , "CV": 09.20, "a":0.05 }},
        "Citraconic": {"type": "gianini", "value": {"DL": float , "CV": 10.00, "a":0.05 }}, # CV arbitraire
        "Glutaric": {"type": "gianini", "value": {"DL": float , "CV": 07.60, "a":0.05 }},
        "Oxoheptanedioic": {"type": "gianini", "value": {"DL": float , "CV": 10.00, "a":0.05 }}, # CV arbitraire
        "MethylSuccinic": {"type": "gianini", "value": {"DL": float , "CV": 10.00, "a":0.05 }}, # CV arbitraire
        "Adipic": {"type": "gianini", "value": {"DL": float , "CV": 05.60, "a":0.05 }},
        "Methylglutaric": {"type": "gianini", "value": {"DL": float , "CV": 10.00, "a":0.05 }}, # CV arbitraire
        "Phtalic": {"type": "gianini", "value": {"DL": float , "CV": 04.30, "a":0.05 }},
        "Pinic": {"type": "gianini", "value": {"DL": float , "CV": 10.00, "a":0.05 }}, # CV arbitraire
        "Suberic": {"type": "gianini", "value": {"DL": float , "CV": 07.10, "a":0.05 }},
        "Azelaic": {"type": "gianini", "value": {"DL": float , "CV": 03.50, "a":0.05 }},
        "Sebacic": {"type": "gianini", "value": {"DL": float , "CV": 08.60, "a":0.05 }},
        # "3-MBTCA": {"type": "percent", "value": 15},
        # "Pinic": {"type": "percent", "value": 15},
        # "Cellulose": {"type": "percent", "value": 15},

        "BNT(2,1)": {"type": "percent", "value": 10},
        "BNT(1,2)": {"type": "percent", "value": 10},
        "BNT(2,3)": {"type": "percent", "value": 10},
        "DNT(2,1)": {"type": "percent", "value": 10},
        "BPT(2,1)": {"type": "percent", "value": 10},
        "Vanilline": {"type": "percent", "value": 10},
        "Acetovanillone": {"type": "percent", "value": 10},
        "Guaiacylacetone": {"type": "percent", "value": 10},
        "Coniferylaldehyde": {"type": "percent", "value": 10},
        "Vanillique": {"type": "percent", "value": 10},
        "Hamovanillique": {"type": "percent", "value": 10},
        "Syringol": {"type": "percent", "value": 10},
        "4-methylsyringol": {"type": "percent", "value": 10},
        "4-propenylsyringol": {"type": "percent", "value": 10},
        "Acetosyringone": {"type": "percent", "value": 10},
        "Syringyl acetone": {"type": "percent", "value": 10},
        "Sinapyl aldehyde": {"type": "percent", "value": 10},
        "Syringic acid": {"type": "percent", "value": 10},
        "Cholesterol": {"type": "percent", "value": 10},

        "Phe": {"type": "percent", "value": 10},
        "Ace": {"type": "percent", "value": 10},
        "For": {"type": "percent", "value": 10},
        "An": {"type": "percent", "value": 10},
        "Fla": {"type": "percent", "value": 10},
        "Pyr": {"type": "percent", "value": 10},
        "Tri": {"type": "percent", "value": 10},
        "Ret": {"type": "percent", "value": 10},
        "BaA": {"type": "percent", "value": 10},
        "Chr": {"type": "percent", "value": 10},
        "BeP": {"type": "percent", "value": 10},
        "BbF": {"type": "percent", "value": 10},
        "BkF": {"type": "percent", "value": 10},
        "BaP": {"type": "percent", "value": 10},
        "BghiP": {"type": "percent", "value": 10},
        "DBahA": {"type": "percent", "value": 10},
        "IP": {"type": "percent", "value": 10},
        "Cor": {"type": "percent", "value": 10},

        "Al": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "As": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Ba": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Be": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Ca": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Cd": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Ce": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Co": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Cr": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Cs": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Cu": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Fe": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "K": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "La": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Li": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Mn": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Mo": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Ni": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Pb": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Rb": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Sb": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Se": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Sn": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Sr": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Ti": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Tl": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "V":  {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Zr": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},
        "Zn": {"type": "gianini", "value": {"DL": float , "CV": 10 , "a":0.15 }},

        "C10": {"type": "percent", "value": 10},
        "C11": {"type": "percent", "value": 10},
        "C12": {"type": "percent", "value": 10},
        "C13": {"type": "percent", "value": 10},
        "C14": {"type": "percent", "value": 10},
        "C15": {"type": "percent", "value": 10},
        "C16": {"type": "percent", "value": 10},
        "C17": {"type": "percent", "value": 10},
        "C18": {"type": "percent", "value": 10},
        "C19": {"type": "percent", "value": 10},
        "C20": {"type": "percent", "value": 10},
        "C21": {"type": "percent", "value": 10},
        "C22": {"type": "percent", "value": 10},
        "C23": {"type": "percent", "value": 10},
        "C24": {"type": "percent", "value": 10},
        "C25": {"type": "percent", "value": 10},
        "C26": {"type": "percent", "value": 10},
        "C27": {"type": "percent", "value": 10},
        "C28": {"type": "percent", "value": 10},
        "C29": {"type": "percent", "value": 10},
        "C30": {"type": "percent", "value": 10},
        "C31": {"type": "percent", "value": 10},
        "C32": {"type": "percent", "value": 10},
        "C33": {"type": "percent", "value": 10},
        "C34": {"type": "percent", "value": 10},
        "C35": {"type": "percent", "value": 10},
        "C36": {"type": "percent", "value": 10},
        "C37": {"type": "percent", "value": 10},
        "C38": {"type": "percent", "value": 10},
        "C39": {"type": "percent", "value": 10},
        "C40": {"type": "percent", "value": 10},
        "2-methylnaphthalene": {"type": "percent", "value": 10},
        "1-methylfluorene": {"type": "percent", "value": 10},
        "3-methylphenanthrene": {"type": "percent", "value": 10},
        "2-methylphenanthrene": {"type": "percent", "value": 10},
        "2-methylanthracene": {"type": "percent", "value": 10},
        "4/9 methylphenanthrene": {"type": "percent", "value": 10},
        "1-methylphenanthrene": {"type": "percent", "value": 10},
        "4-methylpyrene": {"type": "percent", "value": 10},
        "1-methylpyrene": {"type": "percent", "value": 10},
        "2-methylphenanthrene": {"type": "percent", "value": 10},
        "2-methylanthracene": {"type": "percent", "value": 10},
        "4/9 methylphenanthrene": {"type": "percent", "value": 10},
        "1-methylphenanthrene": {"type": "percent", "value": 10},
        "4-methylpyrene": {"type": "percent", "value": 10},
        "1-methylpyrene": {"type": "percent", "value": 10},
        "1+3-methylfluoranthene": {"type": "percent", "value": 10},
        "methylfluoranthene/pyrene": {"type": "percent", "value": 10},
        "methylfluoranthene/pyrene": {"type": "percent", "value": 10},
        "3-methylchrysene": {"type": "percent", "value": 10},
        "methylchrysene/benzoanthracene": {"type": "percent", "value": 10},
        "DBT": {"type": "percent", "value": 10},
        "PheT(4,5)": {"type": "percent", "value": 10},
        "BNT(2,1)": {"type": "percent", "value": 10},
        "BNT(1,2)": {"type": "percent", "value": 10},
        "BNT(2,3)": {"type": "percent", "value": 10},
        "DNT(2,1)": {"type": "percent", "value": 10},
        "BPT(2,1)": {"type": "percent", "value": 10},
        "PAH Sum":     {"type": "percent", "value": 10},
        "Alkanes Sum": {"type": "percent", "value": 10},
        "Alkanes WAX": {"type": "percent", "value": 10},
        "Alkanes noWAX": {"type": "percent", "value": 10},
        "MethPAH Sum": {"type": "percent", "value": 10},
        "PAHs Sum":    {"type": "percent", "value": 10},
        "Hopane Sum":  {"type": "percent", "value": 10},
        "Methoxyphenol Sum": {"type": "percent", "value": 10},
        "MNC Sum":     {"type": "percent", "value": 10},
        "MC Sum":      {"type": "percent", "value": 10},
    }
    return unc

def wrote_conc_and_uncertainties_files(site, programme="_%", species=None,
                                       PMFtype=None, exclude_species=None,
                                       startDate="2010", totalspecie="PMrecons"):
    FILE = "/home/webersa/Documents/BdD/BdD_PM/aerosols.db"
    print("Using {} file".format(FILE))
    print("Starting date: {d}".format(d=startDate))
    print("Total specie: {d}".format(d=totalspecie))


    if species or PMFtype:
        if isinstance(species, list):
            keep_col = species
        elif PMFtype:
            species = pd.read_csv("species4pmf.csv", index_col=0)[PMFtype]
            keep_col = list(species[species].index)  # get only "true values
    else:
        keep_col = get_all_species()

    if exclude_species:
        for sp in exclude_species:
            if sp in keep_col:
                keep_col.remove(sp)


    conn = sqlite3.connect(FILE)
    conc = get_value_from_db(conn, keep_col, site,
                             table="concentration",
                             programme=programme)
    QL = get_value_from_db(conn, keep_col, site,
                           table="QL",
                           programme=programme)
    conn.close()

    conc.date = pd.to_datetime(conc.date)
    conc.dropna(how="all", axis=1, inplace=True)
    conc.set_index('date', drop=True, inplace=True)
    QL = QL.apply(pd.to_numeric, errors='coerce').dropna(how="all", axis=1)

    conc = conc[conc.index>startDate]

    ng2ug = list(set(conc.columns) - set(["date", "PM10", "PM10 corrigées",
                                          "PM2.5 corrigées", "PMrecons", "PM2.5", "EC", "OC"]))

    # CV file:
    # /home/webersa/equipes/chianti/ESPACECERMO/Commun/Limite Detection/CV_DL_LGGE_updated.xlsx
    unc = get_uncertainties()

    # -2: <DL, -1: <QL. Replace them by QL/2
    # print(QL["C20"])
    to_replace = {c: {-2: QL[c].mean()/2, -1: QL[c].mean()/2} for c in QL.mean().index}
    infDLorQL = conc <= 0
    # Replace all negative values to QL/2
    # conc.replace(to_replace, inplace=True)
    conc.loc[:, QL.columns] = conc.loc[:, QL.columns].clip(QL.mean()/2, axis=1)
    # Convert everything to ug
    conc[ng2ug] = conc[ng2ug]/1000
    # Add OC*
    conc['OC*'] = compute_OCstar(conc)
    infDLorQL['OC*'] = False
    # Convert QL to ug
    common_ng_sp = list(set(ng2ug).intersection(QL.columns))
    QL[common_ng_sp] = QL[common_ng_sp]/1000

    # Set value for Giannini et al (assume that DL ~= QL)
    for c in QL.columns:
        if c in unc:
            if unc[c]["type"]=="gianini":
                unc[c]["value"]["DL"] = QL[c].mean()

    # Create the uncertainties dataframe
    df_unc = pd.DataFrame(columns=conc.columns, index=conc.index)
    for c in df_unc.columns:
        # First, set all uncertainties according to % or Giannini
        if c in unc:
            if unc[c]["type"] == "percent":
                df_unc.loc[:, c] = conc[c] * unc[c]["value"]/100 
            elif unc[c]["type"] == "gianini":
                DL = unc[c]["value"]["DL"]
                CV = unc[c]["value"]["CV"]
                a = unc[c]["value"]["a"]
                if DL == float:
                    raise ValueError(
                        """Missing QL in DB for {}, but uncertainty is set """
                        """to 'Gianini': sqrt(DL^2+(x*CV)^2+(x*a)^2).""".format(c)
                    )
                    # df_unc.loc[:, c] = conc[c] * 0.15 
                else:
                    df_unc.loc[:, c] = pd.np.sqrt(
                        (DL**2 + (conc[c]*CV/100)**2 + (conc[c]*a)**2)
                    )
            else:
                raise KeyError("pas le bon type d'unc: {}".format(c))
        else:
            raise KeyError("Uncertainy missing: {}".format(c))
        # Then, handle <QL/<DL
        if any(infDLorQL[c]):
            if c not in QL:
                # raise KeyError(
                print(
                    """Missing QL in DB for {}, but some values are <DL/<QL. """
                    """But, I need to fix the uncertainties to 5/6*QL. """
                    """Obviously there is a problem...""".format(c)
                )
                QLtmp = conc.loc[conc.loc[:, c]>0, c].min()/2
                print("use {} instead".format(QLtmp))
                conc.loc[:, c] = conc.loc[:, c].clip(QLtmp)
                # QLtmp = conc.loc[~infDLorQL[c].values, c].min() / 2
                # print("Missing QL for {} in DB. Use lowest value/2: {}".format(
                #     c, QLtmp
                #     )
                #     )
            else:
                QLtmp = QL[c].mean()
            if c == "C11":
                print(df_unc.loc[infDLorQL[c].values, c])
                print(type(QLtmp))
            df_unc.loc[infDLorQL[c].values, c] = QLtmp * 5/6 #QL[c].mean()
            if c == "C11":
                print(df_unc.loc[infDLorQL[c].values, c])

    # add specie sums
    if ("Arabitol" in conc.columns) and ("Mannitol" in conc.columns):
        conc["Polyols"] = conc[["Arabitol", "Mannitol",]].sum(axis=1)
        df_unc["Polyols"] = df_unc[["Arabitol", "Mannitol",]].sum(axis=1)

    # WAX/noWAX
    if "C20" in conc.columns:
        conc["Alkanes WAX"] = compute_WAX(conc)
        df_unc["Alkanes WAX"] = conc["Alkanes WAX"] * unc["Alkanes WAX"]["value"]/100

    # ==== Remove NaN metals
    if hasattr(conc, "Fe"):
        notnullFe = conc["Fe"].notnull()
        conc =  conc[notnullFe]
        df_unc =  df_unc[notnullFe]
        infDLorQL = infDLorQL[notnullFe]

    # ==== Reindex correctly
    conc = pretty_index(conc)
    df_unc = pretty_index(df_unc)
    infDLorQL = pretty_index(infDLorQL)

    # ==== write to csv file
    conc.columns = conc.columns.str.replace(',','-')
    df_unc.columns = df_unc.columns.str.replace(',','-')
    infDLorQL.columns = infDLorQL.columns.str.replace(',','-')
    conc.replace({pd.np.nan: -999, 0:-999}).to_csv("ready4pmf_{}_conc.csv".format(site))
    # conc.replace({pd.np.nan: -999}).to_csv("ready4pmf_{}_conc.csv".format(site))
    df_unc.replace({pd.np.nan: 999, 0:999}).to_csv("ready4pmf_{}_unc.csv".format(site))
    infDLorQL = infDLorQL.astype(int)
    infDLorQL = infDLorQL.sum() / infDLorQL.shape[0]
    infDLorQL.to_csv("ready4pmf_{}_infDLorQL.csv".format(site))


def main():

    parser = argparse.ArgumentParser(
        description="Create ready-for-PMF file accorging to the DB."
    )

    parser_station = parser.add_argument(
        'station', help='station to extract.'
    )
    parser_programme = parser.add_argument(
        '--programme', help='reasearch programme to extract.',
        default="_%"
    )
    parser_species = parser.add_argument(
        '--specie', help='list of specie to extract',
        default=['OC', 'EC']
    )
    parser_PMFtype = parser.add_argument(
        '--PMFtype', help='preselect species',
        default="SOURCES", choices=["SOURCES", "generic", "MOCOPO"]
    )
    parser_exclude_species = parser.add_argument(
        '--exclude-species', help='List of species to exclude',
        default="SOURCES", nargs='+'
    )
    parser_date = parser.add_argument(
        '--date', help='The starting date',
        default='2010'
    )
    parser_totalspecie = parser.add_argument(
        '--totalspecie', help='The total specie to use',
        default='PMrecons'
    )

    args = parser.parse_args()

    wrote_conc_and_uncertainties_files(
        site=args.station,
        PMFtype=args.PMFtype,
        exclude_species=args.exclude_species,
        programme=args.programme,
        startDate=args.date,
        totalspecie=args.totalspecie
    )


if __name__ == "__main__":
    main()
