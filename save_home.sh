#!/bin/bash

function getGitFolder(){
    find $1 -name .git | sed 's/\/.git//g' > "$2"
}

SRC="$HOME/"
DST="/mnt/webersa/equipes/IGE/sauvegarde/webersa/"
EXCLUDE="$HOME/bin/ExcludeRsync.txt"
EXCLUDEBASE="$HOME/bin/ExcludeRsyncBase.txt"
EXCLUDEGIT="$HOME/bin/ExcludeRsyncGitFolders.txt"
RSYNCFULLLOG="$HOME/bin/rsync.log"
RSYNCLOG="$HOME/bin/backup.log"


# getGitFolder $SRC $EXCLUDEGIT

cat $EXCLUDEBASE > $EXCLUDE
cat $EXCLUDEGIT >> $EXCLUDE

DIR2SAVE=("Documents" "Images" "Vidéos" "bin" "Téléchargements")

# for dir in `ls "$SRC"`
for dir in "${DIR2SAVE[@]}"
do
    dir2save=$SRC$dir
    rsync -a --progress --delete-after --log-file=$RSYNCFULLLOG\
        --exclude-from=$EXCLUDE\
        $dir2save $DST 
    echo `date` "$dir2save: done" >> $RSYNCLOG
done
