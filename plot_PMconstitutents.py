import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
import sqlite3
from matplotlib.ticker import FuncFormatter
from adjustText import adjust_text

plt.interactive(True)

STATION = "VIF"

DATESTART = "2017-02-28"
DATEEND = "2018-02-28"

COLORS = {
    "OM": "#008000",
    "EC": "#000000",
    "Cl-": "#59B2B2",
    "NO3-": "#0000FF",
    "SO42-": "#FF0000",
    "NH4+": "#FF8000",
    "Ca2+": "#CED770",
    "Other ions": "#710077",
    "Metals": "#804000",
    "Anhydrous monosaccharides": "#004000",
    "Organic acids": "#CE9E8E",
    "Polyols": "#A0A015",
    "Oxalate": "#7D0000",
    "MSA": "#2D00BB",
    "Glucose": "#4B8A08",
    "Cellulose": "#0B3B0B",
    "HULIS": "#58ACFA"
}
TEXTCOLORS = {
    "OM": "#000000",
    "EC": "#FFFFFF",
    "Cl-": "#000000",
    "NO3-": "#FFFFFF",
    "SO42-": "#000000",
    "NH4+": "#000000",
    "Ca2+": "#000000",
    "Other ions": "#FFFFFF",
    "Metals": "#FFFFFF",
    "Anhydrous monosaccharides": "#FFFFFF",
    "Organic acids": "#000000",
    "Polyols": "#000000",
    "Oxalate": "#FFFFFF",
    "MSA": "#FFFFFF",
    "Glucose": "#FFFFFF",
    "Cellulose": "#FFFFFF",
    "HULIS": "#000000"
}

ORGANICS = ["HULIS", "Anhydrous monosaccharides", "Polyols", "Organic acids", "Oxalate",
            "MSA", "Glucose", "Cellulose"]

TO_GROUP = {
    "Metals": [
        "Al", "As", "Cd", "Cr", "Cu", "Fe", "Mn", "Mo", "Ni", "Pb", "Rb", "Sb",
        "Se", "Sn", "Ti", "V", "Zn"
    ],
    "Anhydrous monosaccharides":  ["Levoglucosan", "Mannosan", "Galactosan"],
    "Polyols":  ["Arabitol", "Sorbitol", "Mannitol"],
    "Organic acids": [
        "Maleic", "Succinic", "Citraconic", "Glutaric", "Oxoheptanedioic",
        "MethylSuccinic", "Adipic", "Methylglutaric", "3-MBTCA", "Phtalic",
        "Pinic", "Suberic", "Azelaic", "Sebacic"
    ],
    "Other ions": [
        "Na+", "K+", "Mg2+",
    ]
}

TO_MICROGRAMME = ["OM", "EC", "HULIS"]

def format_ions(text):
    map_ions = {
        "Cl-": "Cl$^-$",
        "Na+": "Na$^+$",
        "K+": "K$^+$",
        "NO3-": "NO$_3^-$",
        "NH4+": "NH$_4^+$",
        "SO42-": "SO$_4^{2-}$",
        "Mg2+": "Mg$^{2+}$",
        "Ca2+": "Ca$^{2+}$",
        "nss-SO42-": "nss-SO$_4^{2-}$"
    }
    if text in map_ions.keys():
        return map_ions[text]
    else:
        return text

conn = sqlite3.connect("/home/webersa/Documents/BdD/BdD_PM/db.sqlite")


df = pd.read_sql(
    "SELECT * FROM values_all WHERE station IN ('{}');".format(STATION),
    con=conn
)

df.date = pd.to_datetime(df.date)
df.set_index("date", inplace=True, drop=True)

df = df[(DATESTART < df.index) & (df.index < DATEEND)]


# Metals = [
#     "Al", "As", "Ba", "Cd", "Co", "Cr", "Cs", "Cu", "Fe", "La", "Mn",
#     "Mo", "Ni", "Pb", "Rb", "Sb", "Se", "Sn", "Sr", "Ti", "V", "Zn"
# ]


dff = pd.DataFrame()
for k in TO_GROUP.keys():
    df[k] = df[TO_GROUP[k]].sum(axis=1, min_count=1)

dff = df.reindex(TO_GROUP.keys(), axis=1)
dff["OM"] = df["OC"]*1.8 
to_keep = ["EC", "NO3-", "NH4+", "Cl-", "SO42-", "Ca2+", "Oxalate", "MSA",
           "Glucose", "Cellulose", "HULIS"]
for k in to_keep:
    if k in df.columns:
        dff[k] = df[k]
dff.apply(pd.to_numeric)

for i in TO_MICROGRAMME:
    dff[i] *= 1000

df_proportion_perday = pd.DataFrame()
nonorganics = list(set(dff.columns)-set(ORGANICS))
for c in nonorganics:
    df_proportion_perday[c] = dff[c]/dff[nonorganics].sum(axis=1)

df_proportion_OM_perday = pd.DataFrame()
for c in dff.columns:
    if c in ORGANICS:
        df_proportion_OM_perday[c] = dff[c]/dff["OM"]

d = pd.DataFrame(index=list(df_proportion_OM_perday.columns) +
                 list(df_proportion_perday.columns))

d["other"] = df_proportion_perday.median()
d["organics"] = df_proportion_OM_perday.median()
d.loc[ORGANICS, "other"] = pd.np.nan
df_mg_per_gOM = df_proportion_OM_perday.median() * 1000


order1 = ["OM", "EC", "Cl-", "NO3-", "SO42-", "NH4+", "Ca2+", "Other ions",
          "Metals"]
order2 = ORGANICS.copy()

d = d.reindex(order1+order2)
d.dropna(axis=0, how="all", inplace=True)

OMidentified = df_proportion_OM_perday.median().sum() * 100
dnormalize = d/d.sum() * 100

# d1 = d["other"].reindex(order1, axis=0)
# d2 = d["organics"].reindex(order2, axis=0)
# d1 = d1/d1.sum()
# d2 = d2/d2.sum()


f, ax = plt.subplots(figsize=(9.5,7.5))
dnormalize.T.plot.bar(
    stacked=True,
    color=dnormalize.index.map(COLORS).dropna(),
    rot=0,
    ax=ax,
)

xpos = {"other": 0, "organics": 1}
texts = {"other": [], "organics": []}
for xvar in ["other", "organics"]:
    val = dnormalize[xvar].reset_index().melt(id_vars=["index"])
    cumsum = 0
    for i, v in zip(val["index"], val["value"]):
        if pd.np.isnan(v):
            continue
        cumsum += v
        if xvar == "other":
            annot = ax.annotate("{}".format(format_ions(i)), 
                               (xpos[xvar]-0.28,
                                (cumsum -v/2) ),
                               ha="right",
                               va="center"
                              )
        else:
            text = "{}\n({:.2f} mg.g$_{{OM}}^{{-1}}$)".format(i, 
                                                           df_mg_per_gOM.loc[i])
            if len(text)<40:
                text = text.replace("\n", " ")

            annot = ax.annotate(text, 
                                (xpos[xvar]+0.28,
                                 (cumsum -v/2) ),
                                ha="left",
                                va="center"
                               )
        texts[xvar].append(annot)

        
        ax.annotate("{:.0f}%".format(v),
                    (xpos[xvar], (cumsum - v/2) ),
                    ha="center",
                    va="center",
                    color=TEXTCOLORS[i],
                    fontweight="bold"
                   )
# texts = pd.Series(plt.gcf().get_children()[1].get_children())
# idx = [type(t)==matplotlib.text.Annotation for t in texts]
# texts = texts[idx].tolist()

# adjust_text(texts["other"],
#             arrowprops=dict(arrowstyle="->", color='r', lw=0.5),
#             autoalign='', only_move={'points':'y', 'text':'y'}
#
#                                     )

yOMidentified = OMidentified * dnormalize.loc["OM", "other"]/100
ax.annotate("{:.0f}% identified".format(OMidentified),
            (xpos["other"], yOMidentified/2),
            ha="center",
            va="center",
            color="#FFFFFF",
            fontweight="bold"
           )
ax.plot([0.25, 0.75], [yOMidentified, 100], "-k")
ax.plot([-0.25, 0.25], [yOMidentified, yOMidentified], '-w')

ax.set_title(STATION, fontsize=16)
ax.set_xticklabels([])
f.subplots_adjust(top=0.88,
                 bottom=0.11,
                 left=0.125,
                 right=0.85,
                 hspace=0.2,
                 wspace=0.2)
ax.legend('', frameon=False)
ax.yaxis.set_major_formatter(FuncFormatter('{0:.0f}%'.format))
sns.despine()

